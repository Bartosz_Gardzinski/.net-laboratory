﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1;

namespace jttt
{
    public class Wykonaj
    {
        public string _url;
        public string _atribute;
        public string _email;
        public string _tag { get; set; }

        public Wykonaj()
        { }

        public Wykonaj(string url, string atribute, string email, string tag)
        {
            _url = url;
            _atribute = atribute;
            _email = email;
            _tag = tag;
        }
                
        public void WykonajZadanie()
        {
            Html doc = new Html(_url);
            if (doc.getImages(_atribute))
            {
                Email.SendEmail(_email);
                Email.ZapiszLog(_email);
                File.Delete("obrazek.jpg");
            }
        }


    }
}
