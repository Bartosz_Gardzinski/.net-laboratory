﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.urltext = new System.Windows.Forms.TextBox();
            this.tagtext = new System.Windows.Forms.TextBox();
            this.emailtext = new System.Windows.Forms.TextBox();
            this.SendButton = new System.Windows.Forms.Button();
            this.taskName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.AddButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.SerializeButton = new System.Windows.Forms.Button();
            this.DeserializeButton = new System.Windows.Forms.Button();
            this.tasksTagList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(315, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "JTTT";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 103);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "URL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 227);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 32);
            this.label3.TabIndex = 2;
            this.label3.Text = "TAG";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 413);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(390, 32);
            this.label4.TabIndex = 3;
            this.label4.Text = "E-Mail na który chcesz wysłać";
            // 
            // urltext
            // 
            this.urltext.Location = new System.Drawing.Point(443, 107);
            this.urltext.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.urltext.Name = "urltext";
            this.urltext.Size = new System.Drawing.Size(533, 38);
            this.urltext.TabIndex = 4;
            // 
            // tagtext
            // 
            this.tagtext.Location = new System.Drawing.Point(443, 231);
            this.tagtext.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tagtext.Name = "tagtext";
            this.tagtext.Size = new System.Drawing.Size(533, 38);
            this.tagtext.TabIndex = 5;
            // 
            // emailtext
            // 
            this.emailtext.Location = new System.Drawing.Point(443, 396);
            this.emailtext.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.emailtext.Name = "emailtext";
            this.emailtext.Size = new System.Drawing.Size(533, 38);
            this.emailtext.TabIndex = 6;
            // 
            // SendButton
            // 
            this.SendButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SendButton.Location = new System.Drawing.Point(1153, 655);
            this.SendButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(351, 131);
            this.SendButton.TabIndex = 7;
            this.SendButton.Text = "Wyslij";
            this.SendButton.UseVisualStyleBackColor = true;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // taskName
            // 
            this.taskName.Location = new System.Drawing.Point(443, 543);
            this.taskName.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.taskName.Name = "taskName";
            this.taskName.Size = new System.Drawing.Size(533, 38);
            this.taskName.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 546);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 32);
            this.label5.TabIndex = 9;
            this.label5.Text = "tag zadania";
            // 
            // AddButton
            // 
            this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddButton.Location = new System.Drawing.Point(470, 655);
            this.AddButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(351, 131);
            this.AddButton.TabIndex = 10;
            this.AddButton.Text = "Dodaj";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DeleteButton.Location = new System.Drawing.Point(1540, 655);
            this.DeleteButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(351, 131);
            this.DeleteButton.TabIndex = 11;
            this.DeleteButton.Text = "Usuń";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // SerializeButton
            // 
            this.SerializeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SerializeButton.Location = new System.Drawing.Point(1943, 655);
            this.SerializeButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.SerializeButton.Name = "SerializeButton";
            this.SerializeButton.Size = new System.Drawing.Size(442, 85);
            this.SerializeButton.TabIndex = 13;
            this.SerializeButton.Text = "Serializuj";
            this.SerializeButton.UseVisualStyleBackColor = true;
            this.SerializeButton.Click += new System.EventHandler(this.SerializeButton_Click);
            // 
            // DeserializeButton
            // 
            this.DeserializeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DeserializeButton.Location = new System.Drawing.Point(1943, 785);
            this.DeserializeButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.DeserializeButton.Name = "DeserializeButton";
            this.DeserializeButton.Size = new System.Drawing.Size(442, 85);
            this.DeserializeButton.TabIndex = 14;
            this.DeserializeButton.Text = "Deserializuj";
            this.DeserializeButton.UseVisualStyleBackColor = true;
            this.DeserializeButton.Click += new System.EventHandler(this.DeserializeButton_Click);
            // 
            // tasksTagList
            // 
            this.tasksTagList.FormattingEnabled = true;
            this.tasksTagList.ItemHeight = 31;
            this.tasksTagList.Location = new System.Drawing.Point(1153, 107);
            this.tasksTagList.Name = "tasksTagList";
            this.tasksTagList.Size = new System.Drawing.Size(1232, 469);
            this.tasksTagList.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(2489, 1220);
            this.Controls.Add(this.tasksTagList);
            this.Controls.Add(this.DeserializeButton);
            this.Controls.Add(this.SerializeButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.taskName);
            this.Controls.Add(this.SendButton);
            this.Controls.Add(this.emailtext);
            this.Controls.Add(this.tagtext);
            this.Controls.Add(this.urltext);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox urltext;
        private System.Windows.Forms.TextBox tagtext;
        private System.Windows.Forms.TextBox emailtext;
        private System.Windows.Forms.Button SendButton;
        private System.Windows.Forms.TextBox taskName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button SerializeButton;
        private System.Windows.Forms.Button DeserializeButton;
        private System.Windows.Forms.ListBox tasksTagList;
    }
}

