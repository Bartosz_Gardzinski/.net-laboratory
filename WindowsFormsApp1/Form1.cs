﻿using jttt;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private BindingList<Wykonaj> taskList;
        private string serializeFileName = "serializedTasks.xml";

        public Form1()
        {
            InitializeComponent();

            taskList = new BindingList<Wykonaj>();
            taskList.AllowNew = true;
            taskList.AllowEdit = false;
            taskList.AllowRemove = false;

            tasksTagList.DataSource = taskList;
            tasksTagList.DisplayMember = "_tag";
        }

        private void SendButton_Click(object sender, EventArgs e)
        {

            foreach (Wykonaj task in taskList)
            {
                task.WykonajZadanie();
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            taskList.Clear();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (urltext.Text.Length == 0 || tagtext.Text.Length == 0 || emailtext.Text.Length == 0 || taskName.Text.Length == 0)
            {
                RaiseErrorWindow("Problem z dodaniem zadania!", "Nie podano wystarczającej liczby parametrów dla zadania.");

                return;
            }

            this.taskList.Add(new Wykonaj(urltext.Text, tagtext.Text, emailtext.Text, taskName.Text));

            urltext.Clear();
            tagtext.Clear();
            emailtext.Clear();
            taskName.Clear();
        }

        private void SerializeButton_Click(object sender, EventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BindingList<Wykonaj>));

            Stream writer = new FileStream(this.serializeFileName, FileMode.OpenOrCreate);
            serializer.Serialize(writer, taskList);
            writer.Close();
        }

        private void DeserializeButton_Click(object sender, EventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BindingList<Wykonaj>));

            Stream reader = new FileStream(this.serializeFileName, FileMode.OpenOrCreate);
            try
            {
                var deserializedList = (BindingList<Wykonaj>)serializer.Deserialize(reader);

                if (!deserializedList.Any())
                {
                    RaiseErrorWindow("Problem z deserializacją!", "Brak danych do odczytania");

                    reader.Close();
                    return;
                }

                foreach (Wykonaj task in deserializedList)
                {
                    taskList.Add(task);
                }
            }
            catch
            {
                RaiseErrorWindow("Problem z deserializacją!", "Problem z odczytem z pliku.");
            }
            reader.Close();
        }

        private void RaiseErrorWindow(string title, string message)
        {
            MessageBoxButtons buttons = MessageBoxButtons.OK;

            MessageBox.Show(message, title, buttons);
        }

    }
}
