﻿using System;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    class Html 
    {
        private readonly string _url;

        public Html(string inputUrl)
        {
            this._url = inputUrl;
        }

        public string getPage()
        {
            using (var webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(webClient.DownloadString(_url));

                return html;
            }
        }

        public bool getImages(string atribute)
        {
            try
            {
                var doc = new HtmlAgilityPack.HtmlDocument();

                var pageHtml = getPage();

                doc.LoadHtml(pageHtml);

                var nodes = doc.DocumentNode.Descendants("img");

                foreach (var node in nodes)
                {
                    string alt = node.GetAttributeValue("alt", "").ToLower();

                    if (alt.Contains(atribute.ToLower()))
                    {
                        using (WebClient webClient = new WebClient())
                        {
                            webClient.DownloadFile(node.GetAttributeValue("src", ""), "obrazek.jpg");
                            return true;
                        }
                    }
                }
            }
            catch
            {
                Console.Write("Error catched in downloading images.");
            }

            return false;
        }

    }
}
